package com.example.myprojects;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CallSessionService {
    @FormUrlEncoded
    @POST("remjson.php")
    Call<callSessionResponse> postAccount(@Field("assist") String assist);
}
