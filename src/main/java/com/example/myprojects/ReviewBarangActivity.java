package com.example.myprojects;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ReviewBarangActivity extends AppCompatActivity {
    RatingBar ratingBars;
    String rate;
    EditText review;
    Button send;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_barang);
        ratingBars = (RatingBar)findViewById(R.id.rating);
        review = (EditText)findViewById(R.id.review);
        send = (Button)findViewById(R.id.button);
        ratingBars.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                rate = String.valueOf(v);
            }
        });
        databaseReference = FirebaseDatabase.getInstance().getReference("rating");
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = databaseReference.push().getKey();
                Rating rating = new Rating("Reza", rate, review.getText().toString());
                databaseReference.child(id).setValue(rating);
                Intent intent = new Intent(ReviewBarangActivity.this, HomepageActivity.class);
                startActivity(intent);
                Toast.makeText(ReviewBarangActivity.this, "Review berhasil", Toast.LENGTH_LONG).show();
            }
        });
    }
}
