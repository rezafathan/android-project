package com.example.myprojects;

import android.app.AlertDialog;
import android.app.Application;

import com.alicecallsbob.assist.sdk.config.impl.AssistCobrowseAuthEvent;
import com.alicecallsbob.assist.sdk.config.impl.AssistCobrowseAuthListener;
import com.alicecallsbob.assist.sdk.core.AssistApplication;
import com.alicecallsbob.assist.sdk.core.AssistCore;
import com.alicecallsbob.assist.sdk.core.AssistCoreImpl;

public class SampleAssistApplication extends Application implements AssistApplication {
    private AssistCore assistCore;


    private AlertDialog dialog;

    private AssistCobrowseAuthListener assistCobrowseAuthListener= new AssistCobrowseAuthListener()
    {
        @Override
        public void onCobrowseRequested(AssistCobrowseAuthEvent event)
        {
            setShortCode(null);
            event.acceptCobrowse();
        }
    };

    public AlertDialog getDialog()
    {
        return dialog;
    }

    public void setDialog(AlertDialog dialog)
    {
        this.dialog = dialog;
    }

    public String getShortCode()
    {
        return shortCode;
    }

    public void setShortCode(String shortCode)
    {
        this.shortCode = shortCode;
    }

    private String shortCode;

    @Override
    public void onCreate()
    {
        super.onCreate();

        /** Construct the core in onCreate() */
        assistCore = new AssistCoreImpl(this);
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();

        /** Ensure that the terminate method is called in onTerminate() */
        assistCore.terminate();
        assistCore = null;
    }

    /**
     * Return the core that was constructed in onCreate()
     */
    @Override
    public AssistCore getAssistCore()
    {
        return assistCore;
    }

    public AssistCobrowseAuthListener getAssistCobrowseAuthListener()
    {
        return assistCobrowseAuthListener;
    }


    public void setAssistCobrowseAuthListener(AssistCobrowseAuthListener assistCobrowseAuthListener)
    {
        this.assistCobrowseAuthListener = assistCobrowseAuthListener;
    }
}
