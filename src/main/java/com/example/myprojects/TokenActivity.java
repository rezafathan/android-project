package com.example.myprojects;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.myprojects.authenticator.TOTPAuthenticator;
import com.example.myprojects.configenum.OtpMode;
import com.example.myprojects.exception.TotpException;

public class TokenActivity extends AppCompatActivity {
    Button button;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token);
        button = (Button)findViewById(R.id.generate);
        textView = (TextView) findViewById(R.id.otp);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTOTP();
            }
        });
    }
    private void getTOTP() {
        String strOtp = "";
        String strKey = "757365723139343332363933332d663730652d343963302d393038382d313136343863303763623364";
        Integer timeBlock = Integer.parseInt("30");
        Integer lenght = Integer.parseInt("6");
        try {
            strOtp = TOTPAuthenticator.generateTOTP(strKey,1, timeBlock, lenght);
            textView.setText(strOtp);
        } catch (TotpException e) {
            e.printStackTrace();
            //appendLog(errorToString(e));
            return ;
        }
    }
}
