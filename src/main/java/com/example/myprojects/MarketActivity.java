package com.example.myprojects;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MarketActivity extends AppCompatActivity {
    TextView text_saldo;
    ListView listView;
    List<ListBarang> listBarangList;
    private static final String JSON_URL = "https://api.etherscan.io/api?module=account&action=balancemulti&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&tag=latest&apikey=RMKUZNHAZPPCKZ6FRKUJZSZ8WA5T2ZW7E2";
    private static final String JSON_BARANG = "http://materialcenterikm.com/api/product/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);

        text_saldo = (TextView)findViewById(R.id.text_saldo);
        listView = (ListView)findViewById(R.id.list_view);
        listBarangList = new ArrayList<>();
        loadsaldo();
        loadbarang();
    }


    private void loadsaldo() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject obj = jsonArray.getJSONObject(0);
                            Balance balance = new Balance(obj.getString("account"), obj.getString("balance"));
                            text_saldo.setText(balance.getBalance()+" eth");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void loadbarang(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_BARANG,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ListBarang listBarang = new ListBarang(jsonObject.getString("name"), jsonObject.getString("price_amount"));
                                //Log.e("hasils", jsonObject.getString("name"));
                                listBarangList.add(listBarang);
                            }

                            ListViewAdapter listViewAdapter = new ListViewAdapter(listBarangList, getApplicationContext());
                            listView.setAdapter(listViewAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
