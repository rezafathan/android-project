package com.example.myprojects;

public class Rating {
    String nama, rating, review;

    public Rating(String nama, String rating, String review) {
        this.nama = nama;
        this.rating = rating;
        this.review = review;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
