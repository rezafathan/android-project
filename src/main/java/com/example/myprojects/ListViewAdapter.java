package com.example.myprojects;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<ListBarang> {
    private List<ListBarang> listBarangs;
    private Context mCtx;

    public ListViewAdapter(List<ListBarang> listBarangs, Context mCtx){
        super(mCtx, R.layout.list_view, listBarangs );
        this.listBarangs = listBarangs;
        this.mCtx = mCtx;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View listViewItem = inflater.inflate(R.layout.list_view, null, true);
        TextView textViewName = listViewItem.findViewById(R.id.nama_barang);
        TextView textViewBarang = listViewItem.findViewById(R.id.harga);
        Button beli = listViewItem.findViewById(R.id.beli);
        final ListBarang listBarang = listBarangs.get(position);
        textViewName.setText(listBarang.getName());
        textViewBarang.setText("Harga :  Rp. "+listBarang.getHarga());
        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mCtx, DetailBarangActivity.class);
                intent.putExtra("Name", listBarang.getName());
                intent.putExtra("Harga", listBarang.getHarga());
                mCtx.startActivity(intent);
            }
        });
        return listViewItem;
    }
}
