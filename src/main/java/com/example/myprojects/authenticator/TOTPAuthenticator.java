package com.example.myprojects.authenticator;

import com.example.myprojects.TOTP;
import com.example.myprojects.config.SystemConfig;
import com.example.myprojects.configenum.OtpMode;
import com.example.myprojects.exception.TotpException;


public class TOTPAuthenticator {

	public TOTPAuthenticator() { }
	
	public static void setMode(OtpMode otpMode) {
		SystemConfig.totp_Mode = otpMode.value();
	}
	
	public static OtpMode getMode() {
		return OtpMode.valueOf(SystemConfig.totp_Mode);
	}
	
	public static void setTimeBlock(Integer seconds) {
		SystemConfig.totp_TimeBlock = seconds;
	}
	
	public static Integer getTimeBlock() {
		return SystemConfig.totp_TimeBlock;
	}
	
	public static void setOtpLength(Integer length) throws TotpException {
		if(length > 10 || length <= 0) 
			throw new TotpException("OTP lenght should between 1~10");
		
		SystemConfig.totp_lenght = length;
	}
	
	public static Integer getOtpLength() {
		return SystemConfig.totp_lenght;
	}
	
	public static String generateTOTP(String key) throws TotpException {
		return generateTOTP(key, getMode().value(), getTimeBlock(), getOtpLength());
	}
	
	public static String generateTOTP(String key, Integer mode, Integer timeblock, Integer lenght) throws TotpException {
		String secretKey = key;
		long X = timeblock;
        
    	Long currTime = System.currentTimeMillis()/1000;
    	long T1 = (currTime)/X;
    	String steps = "0";
        steps = Long.toHexString(T1).toUpperCase();
        String strOtp = TOTP.generateTOTP(secretKey, lenght, steps, mode);
		return strOtp;
	}
	
	public static Integer getRemainingTime() {
		return getRemainingTime(getTimeBlock());
	}
	
	public static Integer getRemainingTime(Integer timeBlock) {
		long lTimeBlock = timeBlock;
		long currTime = System.currentTimeMillis()/1000;
    	long startTime = (currTime/lTimeBlock) * lTimeBlock;
//    	System.out.println("timeBlock: " + timeBlock);
//    	System.out.println("currTime: " + currTime);
//    	System.out.println("startTime: " + startTime);
//    	System.out.println("diff: " + (currTime - startTime));
//    	System.out.println("");
    	
    	return (int) (currTime - startTime);
	}
}
