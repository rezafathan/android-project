package com.example.myprojects;

import android.Manifest;
import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.alicecallsbob.assist.sdk.config.AssistConfig;
import com.alicecallsbob.assist.sdk.config.impl.AssistCobrowseListener;
import com.alicecallsbob.assist.sdk.config.impl.AssistConfigBuilder;
import com.alicecallsbob.assist.sdk.config.impl.AssistSharedDocumentReceivedListener;
import com.alicecallsbob.assist.sdk.core.Assist;
import com.alicecallsbob.assist.sdk.core.AssistError;
import com.alicecallsbob.assist.sdk.core.AssistListener;
import com.alicecallsbob.assist.sdk.core.AssistSharedDocument;
import com.alicecallsbob.assist.sdk.core.AssistSharedDocumentClosedEvent;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.X509TrustManager;

import retrofit2.Callback;
import retrofit2.Response;



public class CallActivity extends AppCompatActivity {
    Button calls;
    CallSessionService sessionService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        sessionService = APIutil.getSessionService();
        calls =(Button)findViewById(R.id.call);
        calls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(CallActivity.this,
                        new String[]{android.Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                        1);
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Settings.canDrawOverlays(CallActivity.this)) {
                        call();

                    } else if (!Settings.canDrawOverlays(CallActivity.this)) {
                        checkDrawOverlayPermission(2);
                    }

                } else {
                    Toast.makeText(CallActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                }
            }
            break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkDrawOverlayPermission(int flag) {
        /** check if we already  have permission to draw over other apps */
        if (!Settings.canDrawOverlays(CallActivity.this) && flag == 2) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, 2);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /** check if received result code
         is equal our requested code for draw permission  */
        if (requestCode == 2) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(CallActivity.this)) {
                    call();
                } else {
                    checkDrawOverlayPermission(2);
                }
            }
        }
    }

    private void call()  {
        sessionService.postAccount("assist-123456").enqueue(new Callback<callSessionResponse>() {
            @Override
            public void onResponse(retrofit2.Call<callSessionResponse> call, Response<callSessionResponse> response) {
                if (response.isSuccessful()) {
                    String sessionID = response.body().getSessionid().toString();
                    calls.setEnabled(false);

                    HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
                    AssistConfigBuilder builder = new AssistConfigBuilder(getApplicationContext());
                    builder.setServerPort(8080);
                    builder.setServerHost("183.91.68.181");
                    builder.setConnectSecurely(true);
                    builder.setAgentName("sip:1001@192.168.234.222");
                    builder.setHostnameVerifier(hostnameVerifier);
                    builder.setSessionToken(sessionID);
                    builder.setTrustManager(new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    });

                    AssistSharedDocumentReceivedListener pushListener =  new AssistSharedDocumentReceivedListener() {
                        @Override
                        public void onDocumentReceived(AssistSharedDocument assistSharedDocument) {

                        }

                        @Override
                        public void onError(AssistSharedDocument assistSharedDocument) {
                            Toast.makeText(CallActivity.this, "Can't get the document.", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onDocumentClosed(AssistSharedDocumentClosedEvent assistSharedDocumentClosedEvent) {

                        }
                    };
                    AssistCobrowseListener listener = new AssistCobrowseListener() {
                        @Override
                        public void onCobrowseActive() {
                            //coBrowsingIcon.show();
                        }

                        @Override
                        public void onCobrowseInactive() {
                            //coBrowsingIcon.hide();
                        }
                    };
                    builder.setCobrowseListener(listener);
                    builder.setSharedDocumentReceivedListener(pushListener);
                    // build the AssistConfig
                    AssistConfig config = builder.build();
                    // construct the AssistListener implementation
                    AssistListener listener1 = new AssistListener() {
                        @Override
                        public void onSupportEnded(boolean b) {
                            Toast.makeText(CallActivity.this, "ended", Toast.LENGTH_SHORT).show();
                            calls.setEnabled(true);
                        }

                        @Override
                        public void onSupportError(AssistError assistError, String s) {
                            Toast.makeText(CallActivity.this, "error", Toast.LENGTH_SHORT).show();
                            calls.setEnabled(true);
                        }
                    };
                    // start the support session
                    //getWindow().addFlags(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                    Assist.startSupport(config, getApplication(), listener1);
                    //Assist.startSupport(config, getApplication(), listener1);
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<callSessionResponse> call, Throwable t) {
                Toast.makeText(CallActivity.this,"Service unreachable.1 "+t.toString(), Toast.LENGTH_LONG).show();

            }

        });
    }
}
