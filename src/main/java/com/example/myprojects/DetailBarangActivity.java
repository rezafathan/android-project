package com.example.myprojects;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

public class DetailBarangActivity extends AppCompatActivity {
    TextView nama, harga, cicilan;
    Spinner spinner1, spinner;
    Button bayar;
    Integer c3, c6, c12;
    SeekBar seekBar;
    String value_seek;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang);
        nama = (TextView)findViewById(R.id.nama);
        harga = (TextView)findViewById(R.id.harga);
        nama.setText(getIntent().getStringExtra("Name"));
        harga.setText(getIntent().getStringExtra("Harga"));
        c3 = Integer.valueOf(getIntent().getStringExtra("Harga"))/3;
        c6 = Integer.valueOf(getIntent().getStringExtra("Harga"))/6;
        c12 = Integer.valueOf(getIntent().getStringExtra("Harga"))/12;
        spinner = (Spinner)findViewById(R.id.spinner);
        spinner1 = (Spinner)findViewById(R.id.cara_bayar);
        cicilan = (TextView)findViewById(R.id.cicilan);
        seekBar = (SeekBar)findViewById(R.id.seekbar);
        String[] items1 = new String[]{"Cash", "Cicilan"};
        ArrayAdapter<String>adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items1);
        spinner1.setAdapter(adapter1);
        String[] items = new String[]{"3 Bulan", "6 Bulan", "12 Bulan"};
        ArrayAdapter<String>adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner1.getSelectedItem().equals("Cicilan")){
                    spinner.setVisibility(View.GONE);
                    seekBar.setVisibility(View.VISIBLE);
                    cicilan.setVisibility(View.VISIBLE);
                }
                else {
                    spinner.setVisibility(View.GONE);
                    seekBar.setVisibility(View.GONE);
                    cicilan.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                spinner.setVisibility(View.GONE);
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                value_seek = String.valueOf(i);
                if (value_seek.equals("0")){
                    cicilan.setText("Cicilan 3 Bulan");
                }
                else if(value_seek.equals("1")){
                    cicilan.setText("Cicilan 6 Bulan");
                }
                else if(value_seek.equals("2")){
                    cicilan.setText("Cicilan 12 Bulan");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        bayar = (Button)findViewById(R.id.bayar);
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder alert = new AlertDialog.Builder(DetailBarangActivity.this);
                alert.setTitle("Verifikasi Pembayaran");
                if(spinner1.getSelectedItem().equals("Cash")){
                    alert.setMessage("Total Pembayaran : Rp. "+getIntent().getStringExtra("Harga")).setCancelable(true);
                }
                /*
                else if (spinner1.getSelectedItem().equals("Cicilan")&&spinner.getSelectedItem().equals("3 Bulan")) {
                    alert.setMessage("Total Cicilan : Rp. "+c3).setCancelable(true);
                }
                else if(spinner1.getSelectedItem().equals("Cicilan")&&spinner.getSelectedItem().equals("6 Bulan")){
                    alert.setMessage("Total Cicilan : Rp. "+c6).setCancelable(true);
                }
                else if(spinner1.getSelectedItem().equals("Cicilan")&&spinner.getSelectedItem().equals("12 Bulan")){
                    alert.setMessage("Total Cicilan : Rp. "+c12).setCancelable(true);
                }
                */
                else if (value_seek.equals("0")){
                    alert.setMessage("Total Cicilan : Rp. "+c3).setCancelable(true);
                }
                else if(value_seek.equals("1")){
                    alert.setMessage("Total Cicilan : Rp. "+c6).setCancelable(true);
                }
                else if(value_seek.equals("2")){
                    alert.setMessage("Total Cicilan : Rp. "+c12).setCancelable(true);
                }
                alert.setPositiveButton("Bayar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(DetailBarangActivity.this, OTPActivity.class);
                        intent.putExtra("nama_barang",getIntent().getStringExtra("Name"));
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });

    }

}
