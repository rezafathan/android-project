package com.example.myprojects;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.scottyab.rootbeer.RootBeer;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;

import me.relex.circleindicator.CircleIndicator;
import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class HomepageActivity extends AppCompatActivity {
    TextView username, imei, deviceid, follow, serial;
    TelephonyManager telephonyManager;
    String stringimei, did, name;
    Button chat, saldo;
    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private MyPager myPager;
    LinearLayout linear1,linear2,linear3,linear4, linear5, linear6;
    RootBeer rootBeer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        rootBeer = new RootBeer(this);
        linear1 = (LinearLayout)findViewById(R.id.linear1);
        linear2 = (LinearLayout)findViewById(R.id.linear2);
        linear3 = (LinearLayout)findViewById(R.id.linear3);
        linear4 = (LinearLayout)findViewById(R.id.linear4);
        linear5 = (LinearLayout)findViewById(R.id.linear5);
        linear6 = (LinearLayout)findViewById(R.id.linear6);
        name = getIntent().getStringExtra("username");
        username = (TextView)findViewById(R.id.username);
        username.setText(name);

        imei = (TextView)findViewById(R.id.imei);
        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        stringimei = telephonyManager.getImei();
        imei.setText(stringimei);

        deviceid = (TextView)findViewById(R.id.deviceid);
        deviceid.setText(Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID));
        did = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        follow = (TextView)findViewById(R.id.follow);
        add();

        serial = (TextView)findViewById(R.id.serial);
        serial.setText(Build.SERIAL);

        chat = (Button)findViewById(R.id.chat_button);
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });

        saldo = (Button)findViewById(R.id.saldo);
        saldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, MarketActivity.class);
                startActivity(intent);
            }
        });
        Log.e("adfds", "Refreshed token: " + FirebaseInstanceId.getInstance().getToken());

        FirebaseMessaging.getInstance().subscribeToTopic("news");
        FirebaseMessaging.getInstance().subscribeToTopic("promo");

        myPager = new MyPager(this);
        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(myPager);
        circleIndicator = findViewById(R.id.circle);
        circleIndicator.setViewPager(viewPager);

        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(HomepageActivity.this);
                alert.setTitle("Username Twitter Anda");
                alert.setMessage(name).setCancelable(false).setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });
        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupwrong();
            }
        });
        linear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });
        linear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, MarketActivity.class);
                startActivity(intent);
            }
        });
        linear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, CallActivity.class);
                startActivity(intent);
            }
        });
        linear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, TokenActivity.class);
                startActivity(intent);
            }
        });
        /*
        linear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(HomepageActivity.this);
                alert.setTitle("Serial Number Anda");
                alert.setMessage(Build.MANUFACTURER + " " + Build.MODEL).setCancelable(false).setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });
        linear7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });
        linear8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomepageActivity.this, MarketActivity.class);
                startActivity(intent);
            }
        });
        */
        //ActionBar actionBar = getSupportActionBar(); // or getActionBar();
        //getSupportActionBar().setTitle("BNI Digital Banking"); // set the top title
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setLogo(R.drawable.bni_logo);
        //getSupportActionBar().setDisplayUseLogoEnabled(true);
    }

    public void add(){
        ConfigurationBuilder confbuilder = new ConfigurationBuilder();
        confbuilder.setOAuthAccessToken("158994186-IqpD3f5UdFwlZ9pS1ChQWj5bDx1yopfGM2tEb3YO")
                .setOAuthAccessTokenSecret("sprVpNRBooPGOBZvjbXcMDdCDDYs2epF4hvTLBnly7BzT")
                .setOAuthConsumerKey("ugcigGuqCFSkQtGmMnc91qazh")
                .setOAuthConsumerSecret("65ofToZPYAaMZEoIrkaxVunHSYqW7BiAm5CnJYc7YI9wN95wjJ");
        Twitter twitter = new TwitterFactory(confbuilder.build()).getInstance();

        PagableResponseList<User> followersList;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        try
        {
            followersList = twitter.getFollowersList("rezafathan",-1,100);
            //String s = String.valueOf(followersList.size());
            String s;
            if(followersList.size()<10){
                s = "10";
            }
            else if(followersList.size()>10&&followersList.size()<50){
                s = "50";
            }
            else if(followersList.size()>51&&followersList.size()<70){
                s = "70";
            }
            else {
                s = "90";
            }
            Log.e("listfollow",s);
            follow.setText(s);


        }catch (TwitterException e){
            e.printStackTrace();
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    public void popupwrong(){
        final Dialog dialogwrong = new Dialog(HomepageActivity.this);
        dialogwrong.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogwrong.setContentView(R.layout.popup);
        TextView idimei = (TextView)dialogwrong.findViewById(R.id.idimei);
        TextView iddevice = (TextView)dialogwrong.findViewById(R.id.iddevice);
        TextView idserial = (TextView)dialogwrong.findViewById(R.id.idsn);
        TextView idip = (TextView)dialogwrong.findViewById(R.id.idip);
        TextView idmodel = (TextView)dialogwrong.findViewById(R.id.idmodel);
        TextView idroot = (TextView)dialogwrong.findViewById(R.id.idroot);
        idimei.setText(stringimei);
        iddevice.setText(did);
        idserial.setText(Build.SERIAL);
        idip.setText(getLocalIpAddress());
        idmodel.setText(Build.MANUFACTURER + " " + Build.MODEL);
        if (rootBeer.isRooted()) {
            idroot.setText("Root");
        }
        else {
            idroot.setText("Not Root");
        }

        dialogwrong.show();
    }

}
