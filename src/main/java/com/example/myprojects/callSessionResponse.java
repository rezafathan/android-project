package com.example.myprojects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class callSessionResponse {
    @SerializedName("sessionid")
    @Expose
    private String sessionid;
    @SerializedName("sip")
    @Expose
    private String sip;

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getSip() {
        return sip;
    }

    public void setSip(String sip) {
        this.sip = sip;
    }
}
