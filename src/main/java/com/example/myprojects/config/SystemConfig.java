package com.example.myprojects.config;

import com.example.myprojects.configenum.OtpMode;

public class SystemConfig {
	public static Integer totp_Mode = OtpMode.NUMERIC.value();
	public static Integer totp_TimeBlock = 30;
	public static Integer totp_lenght = 6;
}
