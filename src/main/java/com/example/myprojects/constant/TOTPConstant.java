package com.example.myprojects.constant;

public class TOTPConstant {
	public static final Integer Numeric = 1;
	public static final Integer Alphanumeric = 2;
	public static final Integer Alphabet = 3;
}
