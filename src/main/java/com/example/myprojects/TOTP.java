package com.example.myprojects;
import com.example.myprojects.constant.TOTPConstant;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class TOTP {
	private static final String nums = "0123456789";
    private static final String alphanumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    private TOTP() {}

    /**
     * This method uses the JCE to provide the crypto algorithm.
     * HMAC computes a Hashed Message Authentication Code with the
     * crypto hash algorithm as a parameter.
     *
     * @param crypto: the crypto algorithm (HmacSHA1, HmacSHA256,
     *                             HmacSHA512)
     * @param keyBytes: the bytes to use for the HMAC key
     * @param text: the message or text to be authenticated
     */


    private static byte[] hmac_sha(String crypto, byte[] keyBytes,
            byte[] text){
        try {
            Mac hmac;
            hmac = Mac.getInstance(crypto);
            SecretKeySpec macKey =
                new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (GeneralSecurityException gse) {
            throw new UndeclaredThrowableException(gse);
        }
    }
    
    private static String SHA_512(String msg, String key){
    	String generatedPassword = null;
    	try {
    	     MessageDigest md = MessageDigest.getInstance("SHA-512");
    	     md.update(key.getBytes(StandardCharsets.UTF_8));
    	     byte[] bytes = md.digest(msg.getBytes(StandardCharsets.UTF_8));
    	     StringBuilder sb = new StringBuilder();
    	     for(int i=0; i< bytes.length ;i++){
    	        sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
    	     }
    	     generatedPassword = sb.toString();
    	    } 
    	   catch (NoSuchAlgorithmException e){
    	    e.printStackTrace();
    	   }
    	return generatedPassword;
    }


    /**
     * This method converts a HEX string to Byte[]
     *
     * @param hex: the HEX string
     *
     * @return: a byte array
     */
    private static byte[] hexStr2Bytes(String hex){
        // Adding one byte to get the right conversion
        // Values starting with "0" can be converted
        byte[] bArray = new BigInteger("10" + hex,16).toByteArray();

        // Copy all the REAL bytes, not the "first"
        byte[] ret = new byte[bArray.length - 1];
        for (int i = 0; i < ret.length; i++)
            ret[i] = bArray[i+1];
        return ret;
    }

    private static final long[] DIGITS_POWER
    // 0 1  2   3    4     5      6       7        8		 9			10
    = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000L};

    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     *
     * @return: a numeric String in base 10 that includes
     *              {@link } digits
     */

    private static String generateTOTP(String key,
            String time,
            String returnDigits){
        return generateTOTP4Byte(key, time, returnDigits, "HmacSHA1");
    }


    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     *
     * @return: a numeric String in base 10 that includes
     *              {@link } digits
     */

    private static String generateTOTP256(String key,
            String time,
            String returnDigits){
        return generateTOTP4Byte(key, time, returnDigits, "HmacSHA256");
    }

    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     *
     * @return: a numeric String in base 10 that includes
     *              {@link } digits
     */

    private static String generateTOTP512(String key,
            String time,
            String returnDigits){
        return generateTOTP4Byte(key, time, returnDigits, "HmacSHA512");
    }


    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     * @param crypto: the crypto function to use
     *
     * @return: a numeric String in base 10 that includes
     *              {@link } digits
     */

    private static String generateTOTP4Byte(String key,
            String time,
            String returnDigits,
            String crypto){
        int codeDigits = Integer.decode(returnDigits).intValue();
        String result = null;

        // Using the counter
        // First 8 bytes are for the movingFactor
        // Compliant with base RFC 4226 (HOTP)

        while (time.length() < 16 )
            time = "0" + time;
        
        // Get the HEX in a Byte[]
        byte[] msg = hexStr2Bytes(time);
        byte[] k = hexStr2Bytes(key);

        byte[] hash = hmac_sha(crypto, k, msg);
        // put selected bytes into result int
        int offset = hash[hash.length - 1] & 0xf;

        int binary =
    			((hash[offset] & 0x7f) << 24) |
    			((hash[offset + 1] & 0xff) << 16) |
    			((hash[offset + 2] & 0xff) << 8) |
    			(hash[offset + 3] & 0xff);

        if(!returnDigits.equals("0")) {
        	Long otp = binary % DIGITS_POWER[codeDigits];
            result = Long.toString(otp);
        	while (result.length() < codeDigits) {
                result = "0" + result;
            }
        } else {
        	Long otp = (long) binary;
        	result = Long.toString(otp);
        }
        
        return result;
    }
    
    private static String generateTOTP8Byte(String key,
            String time,
            String returnDigits,
            String crypto){
        int codeDigits = Integer.decode(returnDigits).intValue();
        String result = null;

        // Using the counter
        // First 8 bytes are for the movingFactor
        // Compliant with base RFC 4226 (HOTP)

        while (time.length() < 16 )
            time = "0" + time;
        
        // Get the HEX in a Byte[]
        byte[] msg = hexStr2Bytes(time);
        byte[] k = hexStr2Bytes(key);

        byte[] hash = hmac_sha(crypto, k, msg);
        // put selected bytes into result int
        int offset = hash[hash.length - 1] & 0xf;
        
        long binary =
        		((hash[offset] & 0x7fL) << 56) |
    			((hash[offset + 1] & 0xffL) << 48) |
    			((hash[offset + 2] & 0xffL) << 40) |
    			((hash[offset + 3] & 0xffL) << 32) |
    			((hash[offset + 4] & 0xffL) << 24) |
    			((hash[offset + 5] & 0xffL) << 16) |
    			((hash[offset + 6] & 0xffL) << 8) |
    			(hash[offset + 7] & 0xffL);

        if(!returnDigits.equals("0")) {
        	Long otp = binary % DIGITS_POWER[codeDigits];
            result = Long.toString(otp);
        	while (result.length() < codeDigits) {
                result = "0" + result;
            }
        } else {
        	Long otp = (long) binary;
        	result = Long.toString(otp);
        }
        
        return result;
    }
    
    public static String generateTOTP(String key,
            Integer returnDigits,
            String steps,
            Integer mode
            ) {
    	String result = "";
    	
        while (steps.length() < 16) steps = "0" + steps;
        
    	String seed64 = key;
    	
    	if(TOTPConstant.Numeric.equals(mode)) {
    		String otp = generateTOTP4Byte(seed64, steps, returnDigits.toString(),"HmacSHA1");
    		result = otp;
            while (result.length() < returnDigits) 
                result = "0" + result;
        } else if (TOTPConstant.Alphanumeric.equals(mode)) {
        	String strBinary = generateTOTP8Byte(seed64, steps, "0","HmacSHA256");
        	Long binary = Long.parseLong(strBinary);
        	StringBuilder sbAlpNumOTP = new StringBuilder();
        	Integer alpNumSize = alphanumeric.length();
        	for (int i = 0; i < returnDigits; i++) {
        		int res = getBaseDigit(alpNumSize, i, binary);
        		sbAlpNumOTP.append(alphanumeric.charAt(res));
			}
        	result = sbAlpNumOTP.toString();
        } else if (TOTPConstant.Alphabet.equals(mode)) {
        	String strBinary = generateTOTP8Byte(seed64, steps, "0","HmacSHA256");
        	Long binary = Long.parseLong(strBinary);
        	StringBuilder sbAlpNumOTP = new StringBuilder();
        	Integer alphabetSize = alphabet.length();
        	for (int i = 0; i < returnDigits; i++) {
        		int res = getBaseDigit(alphabetSize, i, binary);
        		sbAlpNumOTP.append(alphabet.charAt((int) res));
			}
        	result = sbAlpNumOTP.toString();
        }
        
    	return result;
    }
    
    private static int getBaseDigit(int base, int digit, long binary) {
    	double b1 = binary%Math.pow(base,digit+1);
		double b2 = binary%Math.pow(base,digit);
		double b = Math.pow(base,digit);
		return (int) ((b1-b2)/b);
    }
}
