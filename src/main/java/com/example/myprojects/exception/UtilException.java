package com.example.myprojects.exception;

public class UtilException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UtilException(String message) {
        super(message);
    }
	
	public UtilException(Exception exception) {
        super(exception);
    }

}
