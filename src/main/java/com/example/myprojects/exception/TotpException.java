package com.example.myprojects.exception;

public class TotpException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TotpException(String message) {
        super(message);
    }
	
	public TotpException(Exception exception) {
        super(exception);
    }


}
