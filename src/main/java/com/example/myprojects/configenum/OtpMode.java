package com.example.myprojects.configenum;

import java.util.HashMap;
import java.util.Map;

public enum OtpMode {
	NUMERIC(1),
    ALPHANUMERIC(2),
    ALPHABET(3);

    private Integer value;

    OtpMode(Integer value) {
        this.value = value;
    }

    private static Map<Integer, OtpMode> map = new HashMap<Integer, OtpMode>();
    static {
        for (OtpMode otpMode : OtpMode.values()) {
            map.put(otpMode.value, otpMode);
        }
    }

    public static OtpMode valueOf(int value) {
        return map.get(value);
    }
    
    public Integer value() {
        return value;
    }
}
