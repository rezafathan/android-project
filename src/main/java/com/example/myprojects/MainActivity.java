package com.example.myprojects;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.scottyab.rootbeer.RootBeer;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    TwitterLoginButton twitterLoginButton;
    TextView ip, device;
    String userid;
    EditText username, password;
    Button button;

    private String URLline = "https://demonuts.com/Demonuts/JsonTest/Tennis/simplelogin.php";
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Twitter.initialize(this);
        setContentView(R.layout.activity_main);
        databaseReference = FirebaseDatabase.getInstance().getReference("log-app");
        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.login);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                userid = String.valueOf(session.getUserId());
                Log.e("tok1", token);
                Log.e("tok2", secret);

                String id = databaseReference.push().getKey();
                LogApp logApp = new LogApp(getLocalIpAddress(),"App",Build.MANUFACTURER + " " + Build.MODEL,"Login","Aktif","Login Berhasil",userid);
                databaseReference.child(id).setValue(logApp);
                login(session);
            }

            @Override
            public void failure(TwitterException exception) {
                String id = databaseReference.push().getKey();
                LogApp logApp = new LogApp(getLocalIpAddress(),"App",Build.MANUFACTURER + " " + Build.MODEL,"Login","Aktif","Login Gagal","0");
                databaseReference.child(id).setValue(logApp);
                Toast.makeText(MainActivity.this, "Gagal", Toast.LENGTH_SHORT).show();
            }
        });
        /*
        RootBeer rootBeer = new RootBeer(this);
        if (rootBeer.isRooted()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Check Root");
            alert.setMessage("Klik Ya Untuk Keluar Aplikasi").setCancelable(false).setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                    System.exit(0);
                }
            });

            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        } else {
            //Toast.makeText(this, "Bukan root", Toast.LENGTH_SHORT).show();
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Check Root");
            alert.setMessage("Aplikasi Anda Tidak di Root").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }
        */
        ip = (TextView) findViewById(R.id.ip);
        ip.setText(getLocalIpAddress());
        device = (TextView) findViewById(R.id.device);
        device.setText(Build.MANUFACTURER + " " + Build.MODEL);

        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username.getText().toString().equals("admin")&&password.getText().toString().equals("123456")){
                    Intent intent = new Intent(MainActivity.this, HomepageActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(MainActivity.this, "Login Gagal", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    public void login(TwitterSession session) {
        String username = session.getUserName();
        Intent intent = new Intent(MainActivity.this, HomepageActivity.class);
        intent.putExtra("username", username);
        String as = Long.toString(session.getUserId());
        Log.e("id", as);
        startActivity(intent);
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    public void postlog(final String string) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLline,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                        parseData(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("userid",string);
                params.put("ip", getLocalIpAddress());
                params.put("device",Build.MANUFACTURER + " " + Build.MODEL);

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void parseData(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equals("true")) {
                JSONArray dataArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < dataArray.length(); i++) {

                    JSONObject dataobj = dataArray.getJSONObject(i);
                    String firstName = dataobj.getString("name");
                    String hobby = dataobj.getString("hobby");
                }

                Intent intent = new Intent(MainActivity.this, HomepageActivity.class);
                startActivity(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}