package com.example.myprojects;

public class ListBarang {
    String name, harga;

    public ListBarang(String name, String harga) {
        this.name = name;
        this.harga = harga;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
