package com.example.myprojects;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import retrofit2.http.POST;

public class ChatActivity extends AppCompatActivity {
    TextView chat;
    EditText text_chat;
    Button add_chat;
    private String a, b;
    DatabaseReference databaseReference;
    NotificationManager NM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        chat = (TextView) findViewById(R.id.chat);
        text_chat = (EditText) findViewById(R.id.text_chat);
        add_chat = (Button) findViewById(R.id.add_chat);

        databaseReference = FirebaseDatabase.getInstance().getReference("project-android-925d7");

        final String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        add_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = databaseReference.push().getKey();
                Message message = new Message(date, text_chat.getText().toString(), "Reza");
                databaseReference.child(id).setValue(message);

            }
        });
        addd();


    }

    private void addd() {
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Post newpost = dataSnapshot.getValue(Post.class);
                for (int i=0; i<8;i++){
                        chat.setText(newpost.message);

                        NotificationManager notif = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        Notification notify = new Notification.Builder
                                (getApplicationContext()).setContentTitle("chat").setContentText(newpost.message).
                                setContentTitle(newpost.names).setSmallIcon(R.drawable.ic_launcher_background).build();
                        notify.priority = Notification.PRIORITY_MAX;
                        //notify.flags |= Notification.FLAG_AUTO_CANCEL;
                        notify.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
                        notif.notify(0, notify);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static class Post {

        public String date;
        public String message;
        public String names;
        public Post(){

        }
        public Post(String date, String message, String names) {
            this.date = date;
            this.message = message;
            this.names = names;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getNames() {
            return names;
        }

        public void setNames(String names) {
            this.names = names;
        }
    }
}
