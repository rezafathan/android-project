package com.example.myprojects;

public class LogApp {
    private String ip;
    private String browser;
    private String device;
    private String function;
    private String status;
    private String message;
    private String userid;

    public LogApp(){

    }
    public LogApp(String ip, String browser, String device, String function, String status, String message, String userid) {
        this.ip = ip;
        this.browser = browser;
        this.device = device;
        this.function = function;
        this.status = status;
        this.message = message;
        this.userid = userid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}

