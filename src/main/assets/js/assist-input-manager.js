;(function () {

    "use strict";

    var inputElements = [];
    var inputDescriptor;
    var inputUid = 0;

    var targetWindow;
    var inputElementsPopulatedCallback;
    var inputElementClickedCallback;
    var inputElementsOnPageCallback;
    var hasPermissionToInteractCallback;
    var AssistControllerInterface;

    var SUPPORTED_INPUT_ELEMENTS = ["INPUT", "SELECT", "TEXTAREA"];
    var INLINE_TEXT_EDITABLE = ["color", "date", "datetime", "datetime-local", "email", "month",
        "number", "range", "search", "tel", "text", "time", "url", "week", "textarea"];

    window.AssistInputManager = {

            init: function (aAssistControllerInterface, aInputElementsPopulatedCallback, aInputElementClickedCallback,
                        aInputElementsOnPageCallback, aHasPermissionToInteractCallback, aTargetWindow) {

            AssistControllerInterface = aAssistControllerInterface;
            inputElementsPopulatedCallback = aInputElementsPopulatedCallback;
            inputElementClickedCallback = aInputElementClickedCallback;
            inputElementsOnPageCallback = aInputElementsOnPageCallback;
            hasPermissionToInteractCallback = aHasPermissionToInteractCallback;
            targetWindow = aTargetWindow || window;
        },

        mapInputElements: function (document) {
            document = document || window.document;
            var hasAssistNoShow = function (element) {
                return element.classList.contains("assist-no-show")
                    || (element.parentElement && hasAssistNoShow(element.parentElement));
            };
            var findInputs = function (element) {
                var inputs = [];
                if (element.childNodes && element.childNodes.length > 0) {
                    for (var i = 0; i < element.childNodes.length; i++) {
                        inputs = inputs.concat(findInputs(element.childNodes[i]));
                    }
                }
                if (isInArray(element.tagName, SUPPORTED_INPUT_ELEMENTS) && !hasAssistNoShow(element) && hasPermissionToInteractCallback(element)) {
                        inputs[inputs.length] = element;
                }
                return inputs;
            };

            var inputs = findInputs(document);
            var labels = document.getElementsByTagName("label");

            var inputDescriptors = [];
            for (var i = 0; i < inputs.length; i++) {
                var input = inputs[i];
                var type = input.tagName == "INPUT" ? input.getAttribute("type") : input.tagName.toLowerCase();
                var label = undefined;
                if (input.id) {
                    for (var j = 0; j < labels.length; j++) {
                        if (labels[j].getAttribute("for") == input.id) {
                                label = labels[j].textContent;
                            break;
                        }
                    }
                }
                if (!label) {
                    label = input.getAttribute("title");
                }
                if (!label && type == "radio") {
                    label = input.value;
                }
                if (!label) {
                    label = input.getAttribute("name");
                }
                if (!label) {
                    label = input.id;
                }
                if (label || type == "select") {
                    var elementInputDescriptor = {
                        type: type,
                        label: label,
                        index: i
                    };
                    switch (type) {
                        case "password":
                        case "hidden":
                        case "image":
                        case "submit":
                            continue;
                        case "radio":
                            elementInputDescriptor.radioGroup = input
                                .getAttribute("name");
                            if (input.checked) {
                                elementInputDescriptor.checked = true;
                            }
                            break;
                        case "checkbox":
                            elementInputDescriptor.checked = input.checked;
                            break;
                        case "select":
                            var options = input.getElementsByTagName("option");
                            var optionDescs = [];
                            for (var j = 0; j < options.length; j++) {
                                optionDescs[optionDescs.length] = {
                                    value: options[j].value,
                                    label: options[j].textContent
                                };
                            }
                            elementInputDescriptor.value = input.value;
                            elementInputDescriptor.options = optionDescs;
                            break;
                        default:
                            var pattern = input.getAttribute("pattern");
                            if (pattern) {
                                elementInputDescriptor.pattern = pattern;
                            }
                            var placeholder = input.getAttribute("placeholder");
                            if (placeholder) {
                                elementInputDescriptor.placeholder = placeholder;
                            }
                            var value = input.value;
                            if (value) {
                                elementInputDescriptor.value = value;
                            }
                            break;
                    }
                    inputDescriptors[inputDescriptors.length] = elementInputDescriptor;

                    input
                        .addEventListener(
                            "change",
                            function () {
                                var inputElement = input;

                                if (!hasPermissionToInteractCallback(inputElement)) {
                                    return;
                                }

                                var descriptor = elementInputDescriptor;
                                var inputType = type;
                                return function () {
                                    switch (inputType) {
                                        case "radio":
                                        case "checkbox":
                                            descriptor.checked = inputElement.checked;
                                            break;
                                        default:
                                            descriptor.value = inputElement.value;
                                            break;
                                    }
                                    var updateDescriptor = {
                                        screenId: inputDescriptor.screenId,
                                        descriptors: [descriptor]
                                    };
                                    var descriptorString = stringifyAndEscape(updateDescriptor);

                                    if (inputElementsPopulatedCallback) {
                                        inputElementsPopulatedCallback(descriptorString);
                                    }
                                }
                            }());
                    if (isInArray(type, INLINE_TEXT_EDITABLE)) {

                        input
                            .addEventListener(
                                "click",
                                function () {
                                    var elementDescriptor = elementInputDescriptor;
                                    return function (event) {
                                        if (event.assist_generated && inputDescriptor) {
                                            var inputElement = inputElements[elementDescriptor.index];
                                            if (!hasPermissionToInteractCallback(inputElement)) {
                                                return;
                                            }

                                            var placeholder = inputElement
                                                .getAttribute("placeholder");
                                            if (placeholder) {
                                                elementDescriptor.placeholder = placeholder;
                                            }
                                            elementDescriptor.value = inputElement.value;
                                            var desc = {
                                                screenId: inputDescriptor.screenId,
                                                clicked: elementDescriptor
                                            };
                                            if (event.assist_source || event.assist_source == 0) {
                                                desc.by = event.assist_source;
                                            }

                                            var descString = stringifyAndEscape(desc);

                                            var bounds = event.target
                                                .getBoundingClientRect();

                                            if (inputElementClickedCallback) {
                                                inputElementClickedCallback(
                                                    descString,
                                                    bounds.left,
                                                    bounds.top);
                                            }
                                        }
                                        if (event.assist_generated) event.preventDefault();
                                    };
                                }());
                    }
                }
            }
            if (inputDescriptors.length > 0) {
                inputElements = inputs;
                var pageId = ++inputUid;
                inputDescriptor = {
                    screenId: pageId,
                    descriptors: inputDescriptors
                };
                var sendInputDescriptor = function () {

                    var jsonPayload = stringifyAndEscape(inputDescriptor);
                    if (inputElementsOnPageCallback) {
                        inputElementsOnPageCallback(jsonPayload);
                    }
                };
                    if (AssistControllerInterface.hasInputTopic() && AssistControllerInterface.inputTopicHasAgent()) {
                    sendInputDescriptor();
                    } else if (AssistControllerInterface.hasInputTopic() == false) {
                        AssistControllerInterface.openInputTopic(
                            function agentJoined() {
                                sendInputDescriptor();
                            },
                                function inputElementsPopulated(
                                        populatedElements) {

                                if (populatedElements.screenId != inputUid) {
                                    return;
                                }
                                var alteredElements = populatedElements.descriptors;
                                for (var i = 0; i < alteredElements.length; i++) {
                                    var altered = alteredElements[i];
                                    var element = inputElements[altered.index];
                                    if (!hasPermissionToInteractCallback(element)) {
                                        continue;
                                    }
                                    var type = element.tagName == "INPUT" ? element.getAttribute("type") : element.tagName.toLowerCase();
                                    switch (type) {
                                        case "radio":
                                        case "checkbox":
                                            element.checked = altered.checked;
                                            break;
                                        default:
                                            element.value = altered.value;
                                            break;
                                    }
                                    if (altered.clickNext) {
                                        for (var i = 0; i < inputDescriptor.descriptors.length; i++) {
                                            if (altered.index == inputDescriptor.descriptors[i].index) {
                                                for (var nextDescriptorIndex = i + 1; nextDescriptorIndex < inputDescriptor.descriptors.length; nextDescriptorIndex++) {
                                                    var nextDescriptor = inputDescriptor.descriptors[nextDescriptorIndex];
                                                    if (nextDescriptor) {
                                                        var nextElement = inputElements[nextDescriptor.index];
                                                        if (nextElement.tagName == "TEXTAREA" ||
                                                            (nextElement.tagName == "INPUT" && (isInArray(nextElement.getAttribute("type"), INLINE_TEXT_EDITABLE)))) {
                                                            var bounds = nextElement
                                                                .getBoundingClientRect();
                                                            var viewWidth = targetWindow.innerWidth;
                                                            var viewHeight = targetWindow.innerHeight;
                                                            var scrollX = (window.pageXOffset === undefined ? targetWindow.document.body.scrollLeft
                                                                : targetWindow.pageXOffset);
                                                            var scrollY = (window.pageYOffset === undefined ? targetWindow.document.body.scrollTop
                                                                : targetWindow.pageYOffset);
                                                            if (bounds.top >= scrollY
                                                                && bounds.bottom <= (scrollY + viewHeight)
                                                                && bounds.left >= scrollX
                                                                && bounds.right <= (scrollX + viewWidth)) {
                                                                nextDescriptor.value = nextElement.value;
                                                                var desc = {
                                                                    screenId: inputDescriptor.screenId,
                                                                    clicked: nextDescriptor
                                                                };
                                                                var descString = stringifyAndEscape(desc);

                                                                if (inputElementClickedCallback) {
                                                                    inputElementClickedCallback(
                                                                        descString,
                                                                        bounds.left,
                                                                        bounds.top);
                                                                }

                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            });
                }
            } else {
                AssistControllerInterface.noInputs();
                inputDescriptor = {};
            }
        }
    };

    function stringifyAndEscape(jsonObject) {
        return unescape(encodeURIComponent(JSON.stringify(jsonObject)));
    }

    function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }

})();